/*
 * Copyright (c) 2021 Jack Viers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.bitbucket.jackcviers

import cats.effect.IO
import fs2.concurrent.Topic
import cats.effect.std

object ErrorHandlingStream {
  def simpleStream(queue: Topic[IO, Int])(implicit
      console: std.Console[IO]
  ): fs2.Stream[IO, Unit] = queue
    .subscribe(5)
    .evalMap { i =>
      if (i % 2 == 0)
        IO.println("I'm failing") *> IO.raiseError[Unit](
          new RuntimeException(s"Purposefully triggering to demo at $i")
        )
      else IO.println(s"I succeded! I got $i")
    }
    .handleErrorWith(_ =>
      simpleStream(queue)
    ) // this fails with a stack overflow after 92,000 errors

}
