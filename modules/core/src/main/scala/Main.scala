/*
 * Copyright (c) 2021 Jack Viers
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package org.bitbucket.jackcviers

import cats.effect.IOApp
import cats.effect.IO
import fs2.concurrent.Topic
import scala.annotation.nowarn

object Main extends IOApp.Simple {

  @nowarn
  val run: IO[Unit] = Topic[IO, Int].flatMap { topic =>
    val publisher =
      fs2.Stream.fromIterator[IO](Iterator.from(1, 1), 1).through(topic.publish)
    val subscriber = ErrorHandlingStream.simpleStream(topic)
    subscriber.concurrently(publisher).evalMap(IO.println).compile.drain
  }

  // for {
  //   topic <- Topic[IO, Int]
  //   publisher = fs2.Stream.constant(1).covary[IO].through(topic.publish)
  //   subscriber = topic.subscribe(10).take(4)
  //   _ = subscriber.concurrently(publisher).map(println).compile.drain
  // } yield ()

}
