# Installing

Add to your `build.sbt`

```scala
libraryDependencies += "org.systemfw" %% "fs2errorhandling" % "@version@"
```

`fs2errorhandling` is published for the following versions of Scala:

@scalaVersions@

