Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / baseVersion := "0.1.0"
ThisBuild / organization := "org.bitbucket.jackcviers"
ThisBuild / publishGithubUser := "jackcviers"
ThisBuild / publishFullName := "Jack Viers"
ThisBuild / homepage := Some(
  url("https://bitbucket.org/jackcviers/fs2errorhandling")
)
ThisBuild / scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/jackcviers/fs2errorhandling"),
    "git@bitbucket.org:jackcviers/fs2errorhandling.git"
  )
)
Global / excludeLintKeys += scmInfo

val Scala213 = "2.13.5"
ThisBuild / spiewakMainBranches := Seq("main")

ThisBuild / crossScalaVersions := Seq(Scala213, "3.0.0-M2", "2.12.10")
ThisBuild / versionIntroduced := Map("3.0.0-M2" -> "3.0.0")
ThisBuild / scalaVersion := (ThisBuild / crossScalaVersions).value.head
ThisBuild / initialCommands := """
  |import cats._, data._, syntax.all._
  |import cats.effect._, concurrent._, implicits._
  |import fs2._
  |import fs2.concurrent._
  |import scala.concurrent.duration._
  |import org.bitbucket.jackcviers._
""".stripMargin

ThisBuild / testFrameworks += new TestFramework("munit.Framework")

def dep(org: String, prefix: String, version: String)(modules: String*)(
    testModules: String*
) =
  modules.map(m => org %% (prefix ++ m) % version) ++
    testModules.map(m => org %% (prefix ++ m) % version % Test)

lazy val root = project
  .in(file("."))
  .enablePlugins(NoPublishPlugin, SonatypeCiReleasePlugin)
  .aggregate(core)

lazy val core = project
  .in(file("modules/core"))
  .settings(
    name := "fs2errorhandling-core",
    headerLicense := Some(HeaderLicense.MIT("2021", "Jack Viers")),
    scalafmtOnCompile := true,
    Compile / run / mainClass := Some("org.bitbucket.jackcviers.Main"),
    libraryDependencies ++=
      dep("org.typelevel", "cats-", "2.6.0")("core")() ++
        dep("org.typelevel", "cats-effect", "3.1.0")("")("-laws") ++
        dep("co.fs2", "fs2-", "3.0.2")("core", "io")() ++
        dep("org.scalameta", "munit", "0.7.25")()("", "-scalacheck") ++
        dep("org.typelevel", "", "1.0.2")()("munit-cats-effect-2") ++
        dep("org.typelevel", "scalacheck-effect", "1.0.1")()("", "-munit")
  )

lazy val docs = project
  .in(file("docs"))
  .settings(
    mdocIn := file("docs"),
    mdocOut := file("target/website"),
    mdocVariables := Map(
      "version" -> version.value,
      "scalaVersions" -> crossScalaVersions.value
        .map(v => s"- **$v**")
        .mkString("\n")
    ),
    githubWorkflowArtifactUpload := false,
    fatalWarningsInCI := false
  )
  .dependsOn(core)
  .enablePlugins(MdocPlugin, NoPublishPlugin)

ThisBuild / githubWorkflowBuildPostamble ++= List(
  WorkflowStep.Sbt(
    List("docs/mdoc"),
    cond = Some(s"matrix.scala == '$Scala213'")
  )
)

ThisBuild / githubWorkflowAddedJobs += WorkflowJob(
  id = "docs",
  name = "Deploy docs",
  needs = List("publish"),
  cond = """
  | always() &&
  | needs.build.result == 'success' &&
  | (needs.publish.result == 'success' || github.ref == 'refs/heads/docs-deploy')
  """.stripMargin.trim.linesIterator.mkString.some,
  steps = githubWorkflowGeneratedDownloadSteps.value.toList :+
    WorkflowStep.Use(
      UseRef.Public("peaceiris", "actions-gh-pages", "v3"),
      name = Some(s"Deploy docs"),
      params = Map(
        "publish_dir" -> "./target/website",
        "github_token" -> "${{ secrets.GITHUB_TOKEN }}"
      )
    )
)
